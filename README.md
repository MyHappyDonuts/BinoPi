# BinoPi



## Raspberry Pi configuration

The Raspberry Pi handles the camera sensor and displays the image through its HDMI port. It needs some configuration before BinoPi can be used. In this project, I used a Raspberry Pi 3B, but other single board computer can be used as well.

### Serial communication

### Libcamera configuration

### Safe shutdown configuration

### Startup with crontab

The libcamera script is executed using crontab : 
```
crontab -e
```
Add the following :
```
@reboot bash /home/username/BinoPi.sh
```
The reboot the system. The image should now be diplayed on startup.

## Revision history

### Version 1.0

### Version 2.0

## Known issues

- Y1 crystal footprint is rotated 90°, but can still be soldered 

## Future improvements

- Silskcreen indicators can be bigger