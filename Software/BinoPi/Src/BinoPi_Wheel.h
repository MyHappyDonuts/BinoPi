/*!
 * \file   BinoPi_Wheel.h
 * \brief  Thumbwheel ADC driver
 * \author Javier Morales
 *
 */

#ifndef BINOPI_WHEEL_H_
#define BINOPI_WHEEL_H_

#include "stm32g0xx_hal.h"

void BinoPi_Wheel_Init(void);
uint8_t BinoPi_Wheel_GetSample(void);

#endif /* BINOPI_WHEEL_H_ */
