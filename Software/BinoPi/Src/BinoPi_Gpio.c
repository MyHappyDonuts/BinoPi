/*!
 * \file   BinoPi_Gpio.c
 * \brief  GPIO driver
 * \author Javier Morales
 *
 */
#include "BinoPi_Gpio.h"

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
stGpio_Type GstLed = {
	.sPort = GPIOB,
	.ui16Pin = 	GPIO_PIN_3,
	.eMode = eMODE_OUTPUT_PP,
	.ePull = eNOPULL,
	.eDefaultState = eSTATE_HIGH
};

uint8_t Gui8Shutdown;

//-----------------------------------------------------------------------------
/* Fonctions */
//-----------------------------------------------------------------------------
void BinoPi_GpioToggle(stGpio_Type* stGpio)
{
	if (stGpio->eMode == eMODE_OUTPUT_PP || stGpio->eMode == eMODE_OUTPUT_OD) {
		HAL_GPIO_TogglePin(stGpio->sPort, stGpio->ui16Pin);
	}
}

void BinoPi_GpioWrite(stGpio_Type* stGpio, eGpioState eState)
{
	if (stGpio->eMode == eMODE_OUTPUT_PP || stGpio->eMode == eMODE_OUTPUT_OD) {
		HAL_GPIO_WritePin(stGpio->sPort, stGpio->ui16Pin, eState);
	}
}

void BinoPi_GpioRead(stGpio_Type* stGpio, eGpioState* eState)
{
	if (stGpio->eMode == eMODE_OUTPUT_PP || stGpio->eMode == eMODE_OUTPUT_OD) {
		 *eState = HAL_GPIO_ReadPin(stGpio->sPort, stGpio->ui16Pin);
	}
}

void BinoPi_GpioInit(void)
{
	HAL_NVIC_DisableIRQ(EXTI2_3_IRQn);

	BinoPi_GpioWrite(&GstLed, GstLed.eDefaultState);
}

void BinoPi_BlinkLed(void)
{
	BinoPi_GpioToggle(&GstLed);
}

uint8_t BinoPi_IsShutdown(void) {
	uint8_t ui8Shutdown = 0;
	BinoPi_GpioRead(&GstLed, &ui8Shutdown);
	return ui8Shutdown;
}
