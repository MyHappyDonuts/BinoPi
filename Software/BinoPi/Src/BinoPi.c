/*!
 * \file   BinoPi.c
 * \brief  Main file
 * \author Javier Morales
 *
 */

#include "BinoPi.h"
#include "BinoPi_WS2812.h"
#include "BinoPi_Wheel.h"
#include "BinoPi_Gpio.h"

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------
#define RED_MAX						255
#define GREEN_MAX					110
#define BLUE_MAX					60

#define ADC_RESOLUTION				255
#define WHEEL_OFF_THRESHOLD			10

#define LED_NUMBER					12

#define PERIOD_RUNNING				5000
#define PERIOD_BLINK_RUNNING		1000
#define PERIOD_BLINK_SHUTDOWN		250
#define PERIOD_BLINK_PICTURE		100
#define PERIOD_LIGHT_UPDATE			20
//#define PERIOD_IS_SHUTDOWN		100

typedef enum {
	eSTARTING = 0,
	eRUNNING = 1,
	eSHUTDOWN = 2,
	ePICTURE = 3
} eSystemState;

//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
uint32_t Gui32Now;
uint32_t Gui32MomentRunning;
uint32_t Gui32MomentBlink;
uint32_t Gui32MomentLightUpdate;
//uint32_t Gui32MomentIsShutdown;

eSystemState Gui8SystemState;
//-----------------------------------------------------------------------------
/* Functions */
//-----------------------------------------------------------------------------
void BinoPi_BlinkTask(void)
{
	BinoPi_BlinkLed();
	switch (Gui8SystemState) {
		case eSTARTING:
		case eRUNNING:
			Gui32MomentBlink = Gui32Now + PERIOD_BLINK_RUNNING;
			break;
		case eSHUTDOWN:
			Gui32MomentBlink = Gui32Now + PERIOD_BLINK_SHUTDOWN;
			break;
		case ePICTURE:
			Gui32MomentBlink = Gui32Now + PERIOD_BLINK_PICTURE;
			break;
		default:
			break;
	}
}

void BinoPi_LighUpdateTask(void)
{
	uint8_t ui8WheelSample = 0;
	static uint8_t ui8WheelSampleOld = 0;
	float fRed = 0;
	float fGreen = 0;
	float fBlue = 0;

	ui8WheelSample = BinoPi_Wheel_GetSample();

	if (ui8WheelSample != ui8WheelSampleOld) {
		fRed = RED_MAX * ui8WheelSample / 255;
		fGreen = GREEN_MAX * ui8WheelSample / 255;
		fBlue = BLUE_MAX * ui8WheelSample / 255;

		BinoPi_SetAllPixelsRgb(fRed, fGreen, fBlue);
		WS2812_Send();

		ui8WheelSampleOld = ui8WheelSample;
	}
}

//void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
//{
//  if (GPIO_Pin == GPIO_PIN_2)
//  {
//	  Gui8SystemState = eSHUTDOWN;
//	  Gui32MomentBlink = Gui32Now;
//	  HAL_NVIC_DisableIRQ(EXTI2_3_IRQn);
//  }
//}

void BinoPi_Init(void)
{
//	BinoPi_FlushBuffer();
//
//	BinoPi_LoadAllPixelsRgb(RED_MAX, GREEN_MAX, BLUE_MAX);
//	BinoPi_SendCommand();
	BinoPi_GpioInit();
	BinoPi_Wheel_Init();

	BinoPi_SetAllPixelsRgb(0, 0, 0);
	WS2812_Send();

	Gui8SystemState = eSTARTING;

	Gui32Now = HAL_GetTick();
	Gui32MomentRunning = Gui32Now + PERIOD_RUNNING;
	Gui32MomentBlink = Gui32Now + PERIOD_BLINK_RUNNING;
	Gui32MomentLightUpdate = Gui32Now + PERIOD_LIGHT_UPDATE;
//	Gui32MomentIsShutdown = Gui32Now + PERIOD_IS_SHUTDOWN;
}

void BinoPi_Main(void){

	Gui32Now = HAL_GetTick();

//	if ((Gui32Now >= Gui32MomentRunning) && (Gui8SystemState == eSTARTING)) {
//		Gui8SystemState = eRUNNING;
//		HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);
//
//	}

	if (Gui32Now >= Gui32MomentBlink) {
		BinoPi_BlinkTask();
	}

	if (Gui32Now >= Gui32MomentLightUpdate) {
		BinoPi_LighUpdateTask();
		Gui32MomentLightUpdate = Gui32Now + PERIOD_LIGHT_UPDATE;
	}

//	if (Gui32Now >= Gui32MomentIsShutdown) {
//		uint8_t ui8ShutdownPin;
//		ui8ShutdownPin = BinoPi_IsShutdown();
//		if (ui8ShutdownPin == 0) {
//			Gui8SystemState = eSHUTDOWN;
//			Gui32MomentIsShutdown = Gui32Now + PERIOD_IS_SHUTDOWN;
//		}
//	}


	HAL_Delay(1);
}
