/*!
 * \file   BinoPi_WS2812.c
 * \brief  WS2812 RGB LED driver
 * \author Javier Morales
 *
 */
#include "BinoPi_WS2812.h"

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------
//#define MAX_LED 24
//#define USE_BRIGHTNESS 0
//
////-----------------------------------------------------------------------------
///* Structs definition */
////-----------------------------------------------------------------------------
//
////-----------------------------------------------------------------------------
///* Global variables */
////-----------------------------------------------------------------------------
#define PIXEL_NUMBER 24

extern TIM_HandleTypeDef htim3;
uint8_t LED_Data[PIXEL_NUMBER][4];
uint16_t pwmData[(24*PIXEL_NUMBER)+50];
int datasentflag = 0;


void BinoPi_SetPixelRgb (int LEDnum, int Red, int Green, int Blue)
{
	LED_Data[LEDnum][0] = LEDnum;
	LED_Data[LEDnum][1] = Green;
	LED_Data[LEDnum][2] = Red;
	LED_Data[LEDnum][3] = Blue;
}

void WS2812_Send (void)
{
	uint32_t indx = 0;
	uint32_t color;

	for (int i= 0; i<PIXEL_NUMBER; i++)
	{
		color = ((LED_Data[i][1]<<16) | (LED_Data[i][2]<<8) | (LED_Data[i][3]));
		for (int i=23; i>=0; i--)
		{
			if (color&(1<<i))
			{
				pwmData[indx] = 53;  // 2/3 of 90
			}

			else pwmData[indx] = 27;  // 1/3 of 90

			indx++;
		}

	}

	for (int i=0; i<50; i++)
	{
		pwmData[indx] = 0;
		indx++;
	}

	HAL_TIM_PWM_Start_DMA(&htim3, TIM_CHANNEL_1, (uint32_t *)pwmData, indx);
	while (!datasentflag){};
	datasentflag = 0;
}

void BinoPi_SetAllPixelsRgb(uint8_t ui8Red, uint8_t ui8Green, uint8_t ui8Blue)
{
	for (uint8_t ui8Led=0; ui8Led<PIXEL_NUMBER; ui8Led++){
		BinoPi_SetPixelRgb(ui8Led, ui8Red, ui8Green, ui8Blue);
	}
}

void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
{
	HAL_TIM_PWM_Stop_DMA(&htim3, TIM_CHANNEL_1);
	datasentflag=1;
}
