/*!
 * \file   BinoPi_Gpio.h
 * \brief  GPIO driver
 * \author Javier Morales
 *
 */

#ifndef BINOPI_GPIO_H_
#define BINOPI_GPIO_H_

#include "stm32g0xx_hal.h"

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------
typedef enum {
	eMODE_OUTPUT_PP,
	eMODE_OUTPUT_OD,
	eMODE_INPUT,
	eMODE_IT,
} eGpioMode;

typedef enum {
	ePULLUP,
	ePULLDOWN,
	eNOPULL
} eGpioPull;

typedef enum {
	eSTATE_LOW,
	eSTATE_HIGH,
} eGpioState;

typedef struct stGpio_Type {
	GPIO_TypeDef* sPort;
	uint16_t ui16Pin;
	eGpioMode eMode;
	eGpioPull ePull;
	eGpioState eDefaultState;
} stGpio_Type;

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Fonction prototypes */
//-----------------------------------------------------------------------------

void BinoPi_GpioInit(void);
void BinoPi_GpioToggle(stGpio_Type* stGpio);
void BinoPi_BlinkLed(void);
uint8_t BinoPi_IsShutdown(void);

#endif /* BINOPI_GPIO_H_ */
