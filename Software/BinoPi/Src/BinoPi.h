/*
 * BinoPi.h
 *
 *  Created on: Feb 11, 2023
 *      Author: javi_
 */

#ifndef BINOPI_H_
#define BINOPI_H_

#include "stm32g0xx_hal.h"

void BinoPi_Init(void);
void BinoPi_Main(void);




#endif /* BINOPI_H_ */
