/*!
 * \file   BinoPi_Wheel.c
 * \brief  Thumbwheel ADC driver
 * \author Javier Morales
 *
 */
#include "BinoPi_Wheel.h"

//-----------------------------------------------------------------------------
/* Constants definiton */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
extern ADC_HandleTypeDef hadc1;

//-----------------------------------------------------------------------------
/* Fonctions */
//-----------------------------------------------------------------------------
void BinoPi_Wheel_Init(void)
{
//	ADC_ChannelConfTypeDef sConfig;

	HAL_ADCEx_Calibration_Start(&hadc1);

//	sConfig.Channel = ADC_CHANNEL_4;
//	HAL_ADC_ConfigChannel(&hadc1, &sConfig);

}

uint8_t BinoPi_Wheel_GetSample(void)
{
	uint8_t ui8WheelSample;
	uint32_t ui32AdcSample;

	HAL_ADC_Start(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 10);

	ui32AdcSample = HAL_ADC_GetValue(&hadc1);
	ui8WheelSample = ui32AdcSample >> 8;

	HAL_ADC_Stop(&hadc1);

	return ui8WheelSample;

}
