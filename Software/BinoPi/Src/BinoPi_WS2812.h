/*!
 * \file   BinoPi_WS2812.h
 * \brief  WS2812 RGB LED driver
 * \author Javier Morales
 *
 */

#ifndef BINOPI_WS2812_H_
#define BINOPI_WS2812_H_

#include "stm32g0xx_hal.h"

//void BinoPi_FlushBuffer(void);
void BinoPi_SetAllPixelsRgb(uint8_t ui8Red, uint8_t ui8Green, uint8_t ui8Blue);
//void BinoPi_SendCommand(void);

void BinoPi_SetPixelRgb (int LEDnum, int Red, int Green, int Blue);
void Set_Brightness (int brightness);
void WS2812_Send (void);
void Reset_LED (void);



#endif /* BINOPI_WS2812_H_ */
